module dht11 #(

    parameter [7:0] ALARM_TEMP_HIGH = 32, // Temperature alarm threshold
    parameter [7:0] ALARM_HUM_HIGH  = 40  // Humidity alarm threshold
)
(
    input logic        clk,
    input logic        rst,

    inout logic        data,     // Bidirectional for communication with DHT11

    output logic [7:0] temp_int, // The temperature value
    output logic [7:0] temp_dec,

    output logic [7:0] hum_int,  // The humidity value
    output logic [7:0] hum_dec,

    //logic              val,

    output logic       hum_alarm,  // Goes high if hum_int is over ALARM_HUM_HIGH threshold
    output logic       temp_alarm
);

    integer TICKS_20MS =  (20000); // timer for start "low" state
    integer TICKS_30US =  (30);    // timer for start "hi" state
    integer TICKS_100US = (100);    // timeout for response signal
    integer TICKS_60US =  (60);    // timeout for "start to transmit 1-bit data" state
    integer TICKS_80US =  (80);    // timeout for data

    // Counters
    logic  poll_ctr;
    logic  start_lo_ctr;
    logic  start_hi_ctr;
    logic  ack_lo_ctr;
    logic  ack_hi_ctr;
    logic  data_lo_ctr;
    logic  data_ctr;
    logic  stop_ctr;


    logic [39:0] data_rec;
    //

    always @ (posedge clk) begin
        if (1) begin
            hum_int  <= data_rec[39-:8];
            hum_dec  <= data_rec[31-:8];
            temp_int <= data_rec[23-:8];
            temp_dec <= data_rec[15-:8];
        end
    end

    //the sequential code for FSM:
    //to be added

    typedef enum logic [7:0] {
    idle_s,
    start_lo_s,
    start_hi_s,
    ack_lo_s,
    ack_hi_s,
    data_start_s,
    data_s,
    stop_s
    } fsm;

    case (fsm)
        idle_s : begin
            if (poll_ctr == 0) begin
                fsm <= start_lo_s;
        end

        start_lo_s : begin
            start_lo_ctr <= (start_lo_ctr == TICKS_20MS) ? 0 : start_lo_ctr + 1;
            if (start_lo_ctr == TICKS_20MS) fsm <= start_hi_s;
        end

        start_hi_s : begin
              start_hi_ctr <= (start_hi_ctr == TICKS_30US) ? 0 : start_hi_ctr + 1;
              if (start_hi_ctr == TICKS_30US) fsm <= ack_lo_s;
        end

        ack_lo_s : begin
            ack_lo_ctr <= (ack_lo_ctr == TICKS_100US) ? 0 : ack_lo_ctr + 1;
            if (pos) begin
                fsm <= ack_hi_s;
            end
        end

        ack_hi_s : begin
            ack_hi_ctr <= (ack_hi_ctr == TICKS_100US) ? 0 : ack_hi_ctr + 1;
            if (neg) begin
            fsm <= data_start_s;
            end
        end

        data_start_s : begin
            data_lo_ctr <= (data_lo_ctr == TICKS_60US) ? 0 : data_lo_ctr + 1;
            if (pos) begin
                fsm <= data_s;
                cur <= 0; // Initially as "0"
            end
            else if (data_lo_ctr == TICKS_60US) begin
                fsm <= idle_s;
            end
        end

        data_s : begin
            data_ctr <= (data_ctr == TICKS_80US) ? 0 : data_ctr + 1;
            if (data_ctr == TICKS_30US) cur <= 1; //bit is "1"
            if (neg) begin
                bit_ctr <= (bit_ctr == 39) ? 0 : bit_ctr + 1;
                fsm <= (bit_ctr == 39) ? stop_s : data_start_s;
        end
        end

        stop_s : begin
            fsm <= idle_s;
        end
    endcase

endmodule